package com.example.isaac.youreit;

import android.content.Intent;
import android.media.session.MediaSession;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.AccessToken;


public class UserDetails extends ActionBarActivity {

    EditText etCity;
    EditText etProvince;
    EditText etCountry;
    EditText etBirthYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        etCity = (EditText) findViewById(R.id.etCity);
        etProvince = (EditText) findViewById(R.id.etProvince);
        etCountry = (EditText) findViewById(R.id.etCountry);
        etBirthYear = (EditText) findViewById(R.id.etBirthYear);

        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runAsyncLogin();
            }
        });
    }

    private void runAsyncLogin() {
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");

        new AsyncLogin(
                extras.getString("id"),
                extras.getString("name"),
                extras.getString("email"),
                extras.getString("token"),
                String.valueOf(etCity.getText()),
                String.valueOf(etProvince.getText()),
                String.valueOf(etCountry.getText()),
                String.valueOf(etBirthYear.getText())
        ).execute();

        Intent intent = new Intent(UserDetails.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
