package com.example.isaac.youreit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class AlarmGetLocation extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new AsyncGetLocation(context, false, intent.getStringExtra("id")).execute();
    }
}