package com.example.isaac.youreit;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

public class AsyncGetLocation extends AsyncTask<Void, Void, String> {

    private TextView myTv;
    private boolean myFlag;
    private Context myContext;
    private MainActivity myActivity;
    private String myId;

    public AsyncGetLocation(MainActivity activity, TextView tv, boolean flag, String id) {
        super();
        myTv = tv;
        myFlag = flag;
        myActivity = activity;
        myId = id;
    }

    public AsyncGetLocation(Context context, boolean flag, String id) {
        myContext = context;
        myFlag = flag;
        myId = id;
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected String doInBackground(Void... params) {
        //do this wherever you are wanting to POST
        URL url;
        HttpURLConnection conn;

        //build the string to store the response text from the server
        String response = "";

        try {
            //if you are using https, make sure to import java.net.HttpsURLConnection
            url = new URL("http://apliko.ca/youreit/getLocation.php");

            //you need to encode ONLY the values of the parameters
            String param = "id=" + URLEncoder.encode(myId, "UTF-8");

            conn = (HttpURLConnection) url.openConnection();
            //set the output to true, indicating you are outputting(uploading) POST data
            conn.setDoOutput(true);
            //once you set the output to true, you don't really need to set the request method to post, but I'm doing it anyway
            conn.setRequestMethod("POST");

            //Android documentation suggested that you set the length of the data you are sending to the server, BUT
            // do NOT specify this length in the header by using conn.setRequestProperty("Content-Length", length);
            //use this instead.
            conn.setFixedLengthStreamingMode(param.getBytes().length);
            conn.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
            //send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(param);
            out.close();

            //start listening to the stream
            Scanner inStream = new Scanner(conn.getInputStream());

            //process the stream and store it in StringBuilder
            while (inStream.hasNextLine())
                response += (inStream.nextLine());
        }
        //catch some error
        catch (MalformedURLException ex) {
            Log.d("MalformedURLException ", "Get --- " + ex.getMessage() + " ---");
        }
        // and some more
        catch (IOException ex) {
            Log.d("IOException ", "Get --- " + ex.getMessage() + " ---");
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != "" && result != null) {
            if (myFlag) {
                if(result.equals("Safe")) {
                    myTv.setText(result);
                    myActivity.setUserInvisible();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        String userid = jsonObject.optString("userid");
                        String name = jsonObject.optString("name");
                        Double distance = Math.sqrt(Double.parseDouble(jsonObject.optString("distance").toString()));
                        String it = jsonObject.optString("it");

                        myTv.setText(String.format("%.1f", distance) + " miles");
                        if (distance <= 1 && it.equals("1"))
                            myActivity.setUserVisible(name, userid);
                        else
                            myActivity.setUserInvisible();

                    } catch (JSONException e) {
                        Log.d("JSONException ", e.getMessage());
                    }
                }
            } else if (!myFlag) {
                try {
                    JSONObject  jsonObject = new JSONObject(result);

                    String userid = jsonObject.optString("userid");
                    String name = jsonObject.optString("name");
                    Double distance = Math.sqrt(Double.parseDouble(jsonObject.optString("distance")));

                    if (distance <= 1) {
                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(myContext)
                                .setSmallIcon(R.drawable.logo)
                                .setContentTitle("You're It")
                                .setContentText(name + ": " + String.format("%.1f", distance) + " miles");

                        Intent resultIntent = new Intent(myContext, MainActivity.class);
                        // Because clicking the notification opens a new ("special") activity, there's
                        // no need to create an artificial back stack.
                        PendingIntent resultPendingIntent = PendingIntent.getActivity(myContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                        mBuilder.setContentIntent(resultPendingIntent);

                        // Sets an ID for the notification
                        int mNotificationId = 001;
                        // Gets an instance of the NotificationManager service
                        NotificationManager mNotifyMgr = (NotificationManager) myContext.getSystemService(myContext.NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.notify(mNotificationId, mBuilder.build());
                    }
                } catch (JSONException e) {e.printStackTrace();}
            }
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}