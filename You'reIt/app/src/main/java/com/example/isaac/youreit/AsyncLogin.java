package com.example.isaac.youreit;

import android.os.AsyncTask;
import android.util.Log;
import com.facebook.AccessToken;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

public class AsyncLogin extends AsyncTask<Void, Void, String> {

    private String myId;
    private String myName;
    private String myEmail;
    private String myAccessToken;
    private String myCity;
    private String myProvince;
    private String myCountry;
    private String myBirthYear;

    public AsyncLogin(String id, String name, String email, String accessToken,
                      String city, String province, String country, String birthYear) {
        myId = id;
        myName = name;
        myEmail = email;
        myAccessToken = accessToken;
        myCity = city;
        myProvince = province;
        myCountry = country;
        myBirthYear = birthYear;
    }

    @Override
    protected void onPreExecute() {
    }

    protected String doInBackground(Void... params) {
        //do this wherever you are wanting to POST
        URL url;
        HttpURLConnection conn;

        //build the string to store the response text from the server
        String response = "";

        try {
            //if you are using https, make sure to import java.net.HttpsURLConnection
            url = new URL("http://apliko.ca/youreit/login.php");

            //you need to encode ONLY the values of the parameters
            String param = "id=" + URLEncoder.encode(myId, "UTF-8") +
                    "&name=" + URLEncoder.encode(myName, "UTF-8") +
                    "&email=" + URLEncoder.encode(myEmail, "UTF-8") +
                    "&accesstoken=" + URLEncoder.encode(myAccessToken, "UTF-8") +
                    "&city=" + URLEncoder.encode(myCity, "UTF-8") +
                    "&province=" + URLEncoder.encode(myProvince, "UTF-8") +
                    "&country=" + URLEncoder.encode(myCountry, "UTF-8") +
                    "&birthyear=" + URLEncoder.encode(myBirthYear, "UTF-8");

            conn = (HttpURLConnection) url.openConnection();
            //set the output to true, indicating you are outputting(uploading) POST data
            conn.setDoOutput(true);
            //once you set the output to true, you don't really need to set the request method to post, but I'm doing it anyway
            conn.setRequestMethod("POST");

            //Android documentation suggested that you set the length of the data you are sending to the server, BUT
            // do NOT specify this length in the header by using conn.setRequestProperty("Content-Length", length);
            //use this instead.
            conn.setFixedLengthStreamingMode(param.getBytes().length);
            conn.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
            //send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(param);
            out.close();

            //start listening to the stream
            Scanner inStream = new Scanner(conn.getInputStream());

            //process the stream and store it in StringBuilder
            while (inStream.hasNextLine())
                response += (inStream.nextLine());
        }
        //catch some error
        catch (MalformedURLException ex) {
            Log.d("MalformedURLException ", "Get --- " + ex.getMessage() + " ---");
        }
        // and some more
        catch (IOException ex) {
            Log.d("IOException ", "Get --- " + ex.getMessage() + " ---");
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {}

    @Override
    protected void onProgressUpdate(Void... values) {}
}