package com.example.isaac.youreit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AlarmSetLocation extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new AsyncSetLocation(context, intent.getStringExtra("id")).execute();
    }
}