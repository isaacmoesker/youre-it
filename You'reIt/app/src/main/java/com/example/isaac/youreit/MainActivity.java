package com.example.isaac.youreit;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;

public class MainActivity extends ActionBarActivity {

    private TextView tvLocation;
    private TextView tvId;
    private Button btnTag;
    private PendingIntent alarmToCancel;
    private AccessToken accessToken;
    private String myUserid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        cancelAlarm(alarmToCancel);

        accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null) {
            Intent intent = new Intent(MainActivity.this, Login.class);
            startActivity(intent);
            finish();
        } else {
            tvLocation = (TextView) findViewById(R.id.tvLocation);
            tvId = (TextView) findViewById(R.id.tvId);
            btnTag = (Button) findViewById(R.id.btnTag);

            Intent intent = new Intent(MainActivity.this, AlarmSetLocation.class);
            intent.putExtra("id", accessToken.getUserId());

            boolean alarmUp = (PendingIntent.getBroadcast(
                    MainActivity.this, 0, intent, PendingIntent.FLAG_NO_CREATE) != null);
            if (!alarmUp) { // if alarm is already active
                startAlarm(intent);
            }

            removeNotification();
        }

        findViewById(R.id.btnTag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTag(MainActivity.this, tvId, accessToken.getUserId(), myUserid).execute();
            }
        });
    }

    private PendingIntent startAlarm(Intent alarmIntent) {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5000, pendingIntent);
        return pendingIntent;
    }

    private void cancelAlarm(PendingIntent pendingIntent) {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
    }

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            /* do what you need to do */
            new AsyncGetLocation(MainActivity.this, tvLocation, true, accessToken.getUserId()).execute();
            /* and here comes the "trick" */
            handler.postDelayed(this, 5000);
        }
    };

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        if (accessToken != null) {
            Intent intent = new Intent(this, AlarmGetLocation.class);
            intent.putExtra("id", accessToken.getUserId());
            alarmToCancel = startAlarm(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        removeNotification();
        cancelAlarm(alarmToCancel);
        if (accessToken != null) {
            handler.postDelayed(runnable, 0);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setUserInvisible() {
        tvId.setVisibility(View.GONE);
        btnTag.setVisibility(View.GONE);
    }

    public void setUserVisible(String name, String userid) {
        tvId.setText(name);
        tvId.setVisibility(View.VISIBLE);
        btnTag.setVisibility(View.VISIBLE);
        myUserid = userid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(001);
    }
}