package com.example.isaac.youreit;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

public class AsyncSetLocation extends AsyncTask<Void, Void, String> {

    private double lat;
    private double lon;
    private String myId;

    public AsyncSetLocation(Context context, String id) {
        super();
        GPSTracker gps = new GPSTracker(context);
        lat = gps.getLatitude();
        lon = gps.getLongitude();
        myId = id;
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected String doInBackground(Void... params) {
        //do this wherever you are wanting to POST
        URL url;
        HttpURLConnection conn;

        //build the string to store the response text from the server
        String response = "";

        try {
            //if you are using https, make sure to import java.net.HttpsURLConnection
            url = new URL("http://apliko.ca/youreit/setLocation.php");

            //you need to encode ONLY the values of the parameters
            String param = "id=" + URLEncoder.encode(myId, "UTF-8") +
                    "&lat=" + URLEncoder.encode(String.valueOf(lat), "UTF-8") +
                    "&lon=" + URLEncoder.encode(String.valueOf(lon), "UTF-8");

            conn = (HttpURLConnection) url.openConnection();
            //set the output to true, indicating you are outputting(uploading) POST data
            conn.setDoOutput(true);
            //once you set the output to true, you don't really need to set the request method to post, but I'm doing it anyway
            conn.setRequestMethod("POST");

            //Android documentation suggested that you set the length of the data you are sending to the server, BUT
            // do NOT specify this length in the header by using conn.setRequestProperty("Content-Length", length);
            //use this instead.
            conn.setFixedLengthStreamingMode(param.getBytes().length);
            conn.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
            //send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(param);
            out.close();

            //start listening to the stream
            Scanner inStream = new Scanner(conn.getInputStream());

            //process the stream and store it in StringBuilder
            while (inStream.hasNextLine())
                response += (inStream.nextLine());
        }
        //catch some error
        catch (MalformedURLException ex) {
            Log.d("MalformedURLException ", "Set --- " + ex.getMessage() + " ---");
        }
        // and some more
        catch (IOException ex) {
            Log.d("IOException ", "Set --- " + ex.getMessage() + " ---");
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {}

    @Override
    protected void onProgressUpdate(Void... values) {}
}