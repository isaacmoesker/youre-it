<?php
	
	if($_POST["id"] && $_POST["name"] && $_POST["email"] && $_POST["accesstoken"] &&
	$_POST["city"] && $_POST["province"] && $_POST["country"] && $_POST["birthyear"]) {
		require_once("dbcon.php");
		
		// HomeTown
		$query = "SELECT id FROM HomeTown WHERE 
				city='".$_POST["city"]."' AND
				province='".$_POST["province"]."' AND
				country='".$_POST["country"]."';";
		$result = $mysqli->query($query);
		
		if ($result && $result->num_rows > 0) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$homeTownID = $row["id"];
			
			$query = "SELECT COUNT(*) AS tally FROM User WHERE hometown=" . $homeTownID;
			$result = $mysqli->query($query);
			
			if ($result && $result->num_rows > 0) {
				$row = $result->fetch_array(MYSQLI_ASSOC);
				
				if($row["tally"] % 5 == 0) {
					setUser($homeTownID, $mysqli, 1);
					$query = "INSERT INTO Token VALUES ('1','".$_POST["id"]."')";
					if (!$mysqli->query($query)) {
						echo $mysqli->error;
					}
				} else {
					setUser($homeTownID, $mysqli, 0);
				}
			} else {
				echo $mysqli->error;
			}
		} else {
			$query = "INSERT INTO HomeTown (city,province,country)
				VALUES ('".$_POST["city"]."','".$_POST["province"]."','".$_POST["country"]."');";

			if ($mysqli->query($query)) {
				setUser($mysqli->insert_id, $mysqli);
			} else {
				echo $mysqli->error;
			}
		}
		
		$mysqli->close();
	} else {
		echo "User vars not set";
	}	
	
	// User
	function setUser($homeTownID, $mysqli, $it) {
		$query = "SELECT EXISTS(SELECT 1 FROM User WHERE id='".$_POST["id"]."') AS bool";
		$result = $mysqli->query($query);
			
		if ($result && $result->num_rows > 0) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			
			if ($row["bool"]) { // If the user exists in the database
			
				$query = "UPDATE User SET 
						name='".$_POST["name"]."', 
						email='".$_POST["email"]."', 
						accesstoken='".$_POST["accesstoken"]."',
						hometown='".$homeTownID."',
						birthyear='".$_POST["birthyear"]."' 
					WHERE id=".$_POST["id"];
				$result = $mysqli->query($query);
				if (!$result && $result->num_rows < 1) {
					echo $mysqli->error;
				}
			} else if (!$row["bool"]) { // If the user does not exist in the database
				$query = "INSERT INTO User VALUES ('".$_POST["id"]."', 
						'".$_POST["accesstoken"]."', 
						'".$it."', 
						'".$_POST["email"]."', 
						'".$_POST["name"]."', 
						'".$homeTownID."',
						'".$_POST["birthyear"]."');";

				if (!$mysqli->query($query)) {
					echo $mysqli->error;
				}
			} else {
				echo "User both exists and does not exist at the same time. Quantum user.";
			}
		} else {
			echo $mysqli->error;
		}
	}
?>