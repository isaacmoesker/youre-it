<?php

	if($_POST["id"]) {
		require_once("dbcon.php");
		$query = "SELECT it FROM User WHERE id = " . $_POST["id"];
		$result = $mysqli->query($query);

		if ($result && $result->num_rows > 0) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$isit = $row["it"];
		
			if ($isit == 1) {
				$table1 = "It";
				$table2 = "NotIt";
			} else if ($isit == 0) {
				$table1 = "NotIt";
				$table2 = "It";
			}
		} else {
			echo $mysqli->error;
		}

		$query = "SELECT lat, lon FROM " . $table1 . " WHERE userid = " . $_POST["id"];
		
		$result = $mysqli->query($query);
		if ($result && $result->num_rows > 0) {
			$row = $result->fetch_array(MYSQLI_ASSOC);

			$distance = 16; // 16 miles
			for ($i = 0; $i < 4; $distance *= 2) {
				$query = "SELECT userid, (SELECT name FROM User WHERE id=userid) AS name,
					POW( 69.1 * ( lat - ".$row["lat"]." ) , 2 ) + 
					POW( 69.1 * ( ".$row["lon"]." - lon ) * COS( lat / 57.3 ) , 2 )
				AS distance, ".$isit." AS it
				FROM " . $table2 . "
				HAVING distance < " . $distance * $distance . "
				ORDER BY distance";
				
				$result = $mysqli->query($query);
				
				if ($result && $result->num_rows > 0) {
					$i = 4;
					$row = $result->fetch_array(MYSQLI_ASSOC);
					echo json_encode($row);
					$mysqli->close();
					exit();
				} else {
					$i++;
				}
			}
			echo "Safe";
				
		} else {
			echo $mysqli->error;
		}
		$mysqli->close();
	} else {
		echo "User ID is not set";
	}
?>