<?php
	
	if($_POST["userid1"] && $_POST["userid2"] && $_POST["lat"] && $_POST["lon"]) {
		require_once("dbcon.php");
		$query = "SELECT userid, id FROM Token WHERE userid=".$_POST["userid1"]." OR userid=".$_POST["userid2"];
		$result = $mysqli->query($query);
			
		if ($result && $result->num_rows > 0) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
		
			if ($row["userid"] == $_POST["userid1"]) { // userid1 is it
				updateUser($_POST["userid1"], 0, $mysqli);
				updateUser($_POST["userid2"], 1, $mysqli);
				updateToken($_POST["userid2"], $row["id"], $mysqli);
				insertTokenHistory($_POST["userid2"], $row["id"], $mysqli);
				deleteLocation($_POST["userid1"], "It", $mysqli);
				deleteLocation($_POST["userid2"], "NotIt", $mysqli);
			} else if ($row["userid"] == $_POST["userid2"]) { // userid2 is it
				updateUser($_POST["userid2"], 0, $mysqli);
				updateUser($_POST["userid1"], 1, $mysqli);
				updateToken($_POST["userid1"], $row["id"], $mysqli);
				insertTokenHistory($_POST["userid1"], $row["id"], $mysqli);
				deleteLocation($_POST["userid2"], "It", $mysqli);
				deleteLocation($_POST["userid1"], "NotIt", $mysqli);
			} else {
				echo "User ID did not match any userid in Token table";
			}
		} else {
			echo $mysqli->error;
		}
	} else {
		echo "User IDs are not set";
	}
   		
   	function updateUser($userid, $bool, $mysqli) {
		$query = "UPDATE User SET it=".$bool." WHERE id=".$userid;
		if (!$mysqli->query($query)) {
			echo $mysqli->error;
		}
	}
	
	function updateToken($userid, $tokenid, $mysqli) {
		$query = "UPDATE Token SET userid=".$userid." WHERE id=".$tokenid;
		if (!$mysqli->query($query)) {
			echo $mysqli->error;
		}
	}
		
	function insertTokenHistory($userid, $tokenid, $mysqli) {
		$query = "INSERT INTO TokenHistory (userid, tokenid, lat, lon)
			VALUES (".$userid.", ".$tokenid.", ".$_POST["lat"].", ".$_POST["lon"].")";
		if (!$mysqli->query($query)) {
			echo $mysqli->error;
		}
	}
	
	function deleteLocation($userid, $table, $mysqli) {
		$query = "DELETE FROM ".$table." WHERE userid='".$userid."';";
		if (!$mysqli->query($query)) {
			echo $mysqli->error;
		}
	}

?>